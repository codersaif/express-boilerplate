app.controller('loginController', ['$scope', '$http',
    function ($scope, $http) {
        console.log('from loginController');

        $scope.submitUser = function () {
            console.log($scope.pwd, $scope.userId);
            $http.post('/users/signin', {
                userId: $scope.userId,
                pwd: $scope.pwd
            }).then(function (response) {
                console.log(response.data);
            }, function (error) {
                console.log(error);
            });
        };

    }]);