var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserModel = new Schema({
    userId: {type: String, unique: true},
    password: {type: String},
    privilege: [{type: String,  enum: ['admin', 'teacher', 'student', 'staff', 'guardian']}],
    salt: {type: String},
    firstName: {type: String},
    lastName: {type: String},
    phoneNumber: {type: String},
    addressLine1: {type: String},
    addressLine2: {type: String},
    dateOfBirth: {type: Date}

});

module.exports = mongoose.model('User', UserModel);


// student can change password
// can't register themselves
// can see routines
// exam schedule
// can see message
// can take subjects