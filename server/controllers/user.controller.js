var mongoose = require('mongoose');
var passport = require('passport');
require('../models/User');
var User = mongoose.model('User');


exports.signin = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/login');
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }
            return res.redirect('/users/' + user.userId);
        });
    })(req, res, next);
};