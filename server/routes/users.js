var express = require('express');
var router = express.Router();
var controller = require('../controllers/user.controller.js');
var passport = require('passport');

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});
/*
router.post('/signin',
    passport.authenticate('local', {
        successRedirect: '/student',
        failureRedirect: '/',
        failureFlash: true
    }));*/

router.post('/signin',
    passport.authenticate('local'),
    function(req, res) {
        // If this function gets called, authentication was successful.
        // `req.user` contains the authenticated user.
        res.redirect('/students/' + req.user.username);
    });


module.exports = router;
