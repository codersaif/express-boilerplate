app.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/");
    //
    // Now set up the states

    $stateProvider
        .state('index', {
            url: '/',
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/home.html"},
                "side": {templateUrl: "views/side.html"}
            }
        })


        .state('about', {
            url: "/about",
            abstract: true,
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/about/about.html"},
                "side": {templateUrl: "views/side.html"}
            }
        }).state('about.missionvision', {
            url: "/missionvision",
            views: {
                "content@": {templateUrl: "views/about/mission-vision.html"},
            }
        }).state('about.organogram', {
            url: "/organogram",
            views: {
                "content@": {templateUrl: "views/about/organogram.html"},
            }
        }).state('about.duties', {
            url: "/duties",
            views: {
                "content@": {templateUrl: "views/about/duties.html"}
            }
        }).state('about.manpower', {
            url: "/manpower",
            views: {
                "content@": {templateUrl: "views/about/manpower.html"}
            }
        }).state('about.acknowledgement', {
            url: "/acknowledgement",
            views: {
                "content@": {templateUrl: "views/about/acknowledgement.html"}
            }
        })


        .state('students', {
            url: "/students",
            abstract: true,
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/students/students.html"},
                "side": {templateUrl: "views/side.html"}
            }
        }).state('students.eligibility', {
            url: "/eligibility",
            views: {
                "content@": {templateUrl: "views/students/eligibility.html"}
            }
        }).state('students.notice', {
            url: "/notice",
            views: {
                "content@": {templateUrl: "views/students/notice.html"}
            }
        }).state('students.news', {
            url: "/news",
            views: {
                "content@": {templateUrl: "views/students/news.html"}
            }
        })


        .state('courses', {
            url: "/courses",
            abstract: true,
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/courses/courses.html"},
                "side": {templateUrl: "views/side.html"}
            }
        }).state('courses.offered', {
            url: "/offered",
            views: {
                "content@": {templateUrl: "views/courses/offered.html"}
            }
        }).state('courses.examschedule', {
            url: "/examschedule",
            views: {
                "content@": {templateUrl: "views/courses/examschedule.html"}
            }
        }).state('courses.curriculum', {
            url: "/curriculum",
            views: {
                "content@": {templateUrl: "views/courses/curriculum.html"}
            }
        })


        .state('results', {
            url: "/results",
            abstract: true,
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/results/results.html"},
                "side": {templateUrl: "views/side.html"}
            }
        }).state('results.transcript', {
            url: "/transcript",
            views: {
                "content@": {templateUrl: "views/results/transcript.html"}
            }
        }).state('results.summary', {
            url: "/summary",
            views: {
                "content@": {templateUrl: "views/results/summary.html"}
            }
        })


        .state('affiliates', {
            url: "/affiliates",
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/affiliates.html"},
                "side": {templateUrl: "views/side.html"}
            }
        })


        .state('forms', {
            url: "/forms",
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/forms/forms.html"},
                "side": {templateUrl: "views/side.html"}
            }
        }).state('forms.publications', {
            url: "/publications",
            views: {
                "content@": {templateUrl: "views/forms/publications.html"}
            }
        })


        .state('contact', {
            url: "/contact",
            views: {
                "navigation": {templateUrl: "views/navigation.html"},
                "content": {templateUrl: "views/contact.html"
                , controller: 'contactController'},
                "side": {templateUrl: "views/side.html"}
            }
        });
});
