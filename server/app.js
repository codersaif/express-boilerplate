var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swig = require('swig');
var passport = require('passport');
// Bring Mongoose into the app
var mongoose = require( 'mongoose' );
var session = require('express-session');
// Build the connection string
var dbURI = 'mongodb://localhost/DIU'
    , LocalStrategy = require('passport-local').Strategy;

var routes = require('./routes/index');
var users = require('./routes/users');
var admins = require('./routes/admins');
var teachers = require('./routes/teachers');
var students = require('./routes/students');

var app = express();

// Create the database connection
mongoose.connect(dbURI);

// view engine setup
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');


passport.use(new LocalStrategy(
    function(username, password, done) {
        console.log('from app.js');
        User.findOne({ userId: username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            if (!user.validPassword(password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, user);
        });
    }
));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({ secret: 'keyboard cat', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());



app.use('/', routes);
app.use('/users', users);
app.use('/admins', admins);
app.use('/teachers', teachers);
app.use('/students', students);


app.use(
    express.static(path.join(__dirname, '../public'),
        {'Cache-Control': 'no-cache'}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('pages/error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('pages/error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
